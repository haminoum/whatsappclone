import './App.css';
import ChatsListScreen from './components/ChatsListScreen';

function App() {
  return (
    <div>
      <ChatsListScreen />
    </div>
  );
}

export default App;
